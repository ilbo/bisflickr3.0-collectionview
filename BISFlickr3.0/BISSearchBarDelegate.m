//
//  BISSearchBarDelegate.m
//  BISFlickr3.0
//
//  Created by iliya on 25.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BISSearchBarDelegate.h"
#import "BISCollectionController.h"
#import "BISUserDefaults.h"
#import "BISSharedCache.h"
#import "UIColor+BISUIColorCategory.h"

@implementation BISSearchBarDelegate

- (instancetype)initWithController:(BISCollectionController *)controller {
    self = [super init];
    if (self) {
        _controller = controller;
    }
    return self;
}

- (void)addSearchBar {

    _searchBar = [[UISearchBar alloc] init];
    // меняем цвет серч бара
    UITextField *searchField = [_searchBar valueForKey:@"searchField"];
    UIColor *searchColor = [UIColor bisSearchColorWithAlpha:1.0];
    searchField.backgroundColor = searchColor;
    searchField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    searchField.borderStyle = UITextBorderStyleRoundedRect;
    _searchBar.placeholder = @"Поиск";
    _searchBar.delegate = self;
    [_searchBar becomeFirstResponder];

    //добавляем кнопку настроек справа от серч бара
    UIImage *icSettings = [UIImage imageNamed:@"icSettings"];
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] initWithImage:icSettings style:UIBarButtonItemStylePlain target:nil action:nil];
    barButton.tintColor = [UIColor blackColor];
    _controller.navigationItem.rightBarButtonItem = barButton;
    _controller.navigationController.navigationBar.barTintColor = [UIColor bisBackgroundColorWithAlpha:0.9];
    _controller.navigationController.navigationBar.translucent = NO;
    _controller.navigationItem.titleView = _searchBar;
}

/**
 Обработка нажатия Enter на поисковом элементе
 Запускаем таск для общих данных c блоком для получения словаря с общими данными из таска
 */
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar endEditing:YES];
    if (_controller.queue != nil) {
        [_controller.queue cancelAllOperations];
    }
    _controller.queue = [NSOperationQueue new];
    _controller.cache = [BISSharedCache new];
    _controller.executingOperations = [NSMutableDictionary new];
    _controller.pictures = [NSArray new];

    [BISUserDefaults saveLastRequestLine:searchBar.text];
    [_controller sendCommonRequest:[_controller getOptimizedSearchText:searchBar.text]];
}

@end
