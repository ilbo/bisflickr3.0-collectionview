//
//  BISPicture.h
//  BISFlickr3.0
//
//  Created by iliya on 25.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BISUser;

/**
 Список статусов загрузки фотографии

 - BISDownloadStateNone: Не загружалась
 - BISDownloadStateDownloading: В процессе загрузки
 - BISDownloadStateDownloaded: Загружена
 - BISDownloadStateError: Ошибка в процессе загузки
 */
typedef NS_ENUM(NSInteger, BISDownloadState) {
    BISDownloadStateNone,
    BISDownloadStateDownloading,
    BISDownloadStateDownloaded,
    BISDownloadStateError
};

@interface BISPicture : NSObject

@property(nonatomic) BISDownloadState downloadState;

/*! ID фотографии, присвоенный на сервере */
@property(nonatomic, assign, readonly) NSUInteger identifier;
/*! Владелец фотографии*/
@property(nonatomic, strong) BISUser *owner;
/*! Индекс фотографии в коллекции */
@property(nonatomic, assign, readonly) NSUInteger index;
/*! Ссылка на фотографию */
@property(nonatomic, copy, readonly) NSURL *url;
/*! Прогресс загрузки */
@property(nonatomic, assign, readwrite) float progress;
/*! Размер фотографии */
@property(nonatomic, copy, readwrite) NSString *size;
/*! Местоположение, где сделана фотография */
@property(nonatomic, copy, readonly) NSString *location;
/*! Описание фотографии */
@property(nonatomic, copy, readonly) NSString *title;
/*! Количество комментариев */
@property(nonatomic, assign) NSUInteger countComments;
/*! Количество лайков */
@property(nonatomic, assign) NSUInteger countLikes;
/*! Словарь из пользователей и их комментариев */
@property(nonatomic, copy) NSDictionary<BISUser *, NSString *> *comments;
/*! Список, кому понравилась фотография */
@property(nonatomic, copy) NSArray<BISUser *> *whoLiked;


/**
 Создает массив объектов из словаря

 @param dictionary Словарь, который пришел от сервера
 @return Массив объектов Picture
 */
+ (NSMutableArray<BISPicture *> *)picturesFromDictionary:(NSDictionary *)dictionary;

/**
 Добавляет данные(владельца,локацию,кол-во комментариев) в объект из словаря

 @param picture Объект Picture
 @param dictionary Словарь, полученный по запросу method=flickr.photos.getInfo
 @return Обновленный объект Picture
 */
+ (BISPicture *)getPictureWithUpdateInfo:(BISPicture *)picture fromDict:(NSDictionary *)dictionary;

/**
 Добавляет пользователей, которые лайкнули и их кол-во из словаря

 @param picture Объект Picture
 @param dictionary Словарь, полученный по запросу method=flickr.photos.getFavorites
 @return Обновленный объект Picture
 */
+ (BISPicture *)getPictureWithUpdateFavorites:(BISPicture *)picture fromDict:(NSDictionary *)dictionary;

/**
 Добавляет пользователей и их комментарии из словаря.

 @param picture Объект Picture
 @param dictionary Словарь, полученный по запросу method=flickr.photos.comments.getList
 @return Обновленный объект Picture
 */
+ (BISPicture *)getPictureWithUpdateComments:(BISPicture *)picture fromDict:(NSDictionary *)dictionary;

@end
