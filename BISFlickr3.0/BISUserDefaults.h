//
//  BISUserDefaults.h
//  BISFlickr3.0
//
//  Created by iliya on 08.07.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BISUserDefaults : NSObject

+ (nullable NSString *)getLastRequestLine;

+ (void)saveLastRequestLine:(nonnull NSString *)lastRequestLine;

@end
