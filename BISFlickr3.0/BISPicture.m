//
//  BISPicture.m
//  BISFlickr3.0
//
//  Created by iliya on 25.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISPicture.h"
#import "BISUrlBuilder.h"
#import "BISUser.h"

@interface BISPicture ()

/*! Местоположение, где сделана фотография, открыта для записи только внутри класса*/
@property(nonatomic, copy, readwrite) NSString *location;
/*! Описание фотографии, открыто для записи только внутри класса */
@property(nonatomic, copy, readwrite) NSString *title;

@end

@implementation BISPicture

- (instancetype)initWithDictionary:(NSDictionary *)dictionary index:(NSInteger)index {
    NSString *identifier = dictionary[@"id"];
    NSString *secret = dictionary[@"secret"];
    NSString *serverId = dictionary[@"server"];
    NSString *farmId = [NSString stringWithFormat:@"%@", dictionary[@"farm"]];
    NSString *tilte = dictionary[@"title"];

    self = [super init];
    if (self) {
        _downloadState = BISDownloadStateNone;
        _identifier = [identifier integerValue];
        _index = index;
        _title = tilte;
        _url = [BISUrlBuilder getUrlString:farmId serverId:serverId identifier:identifier secret:secret];
        _progress = 0.0;
        _size = @"0KB";
    }
    return self;
}

+ (NSMutableArray<BISPicture *> *)picturesFromDictionary:(NSDictionary *)dictionary {
    NSDictionary *commonDictionaries = dictionary[@"photos"];
    NSArray *photoDictionary = commonDictionaries[@"photo"];

    NSMutableArray *pictures = [NSMutableArray new];
    for (NSInteger i = 0; i < photoDictionary.count; i++) {
        BISPicture *picture = [[BISPicture alloc] initWithDictionary:photoDictionary[i] index:i];
        [pictures addObject:picture];
    }
    return pictures;
}

+ (BISPicture *)getPictureWithUpdateInfo:(BISPicture *)picture fromDict:(NSDictionary *)dictionary {
    NSDictionary *ownerDictionary = dictionary[@"photo"][@"owner"];
    NSDictionary *commentsDictionary = dictionary[@"photo"][@"comments"];
    NSDictionary *localityDictionary = dictionary[@"photo"][@"location"][@"locality"];

    NSString *userName = ownerDictionary[@"username"];
    NSString *iconfarm = [NSString stringWithFormat:@"%@", ownerDictionary[@"iconfarm"]];
    NSString *iconserver = [NSString stringWithFormat:@"%@", ownerDictionary[@"iconserver"]];
    NSString *nsid = ownerDictionary[@"nsid"];
    NSString *location = localityDictionary[@"_content"];
    NSString *countComments = commentsDictionary[@"_content"];

    BISPicture *updatedPicture = picture;
    BISUser *owner = [[BISUser alloc] initWithUserName:userName urlAvatar:[BISUrlBuilder getAvatarUrl:iconfarm iconserver:iconserver nsidOrAuthor:nsid]];
    updatedPicture.owner = owner;
    if (location) {
        updatedPicture.location = location;
    } else {
        updatedPicture.location = @"none";
    }
    updatedPicture.countComments = [countComments integerValue];

    return updatedPicture;
}

+ (BISPicture *)getPictureWithUpdateFavorites:(BISPicture *)picture fromDict:(NSDictionary *)dictionary {
    NSArray *personsWhoLikedArray = dictionary[@"photo"][@"person"];
    NSString *countFavorites = [NSString stringWithFormat:@"%@", dictionary[@"photo"][@"total"]];

    NSMutableArray < BISUser * > *whoLiked = [NSMutableArray new];
    for (NSDictionary *userDictionary in personsWhoLikedArray) {
        NSString *userName = userDictionary[@"username"];
        NSString *iconfarm = [NSString stringWithFormat:@"%@", userDictionary[@"iconfarm"]];
        NSString *iconserver = [NSString stringWithFormat:@"%@", userDictionary[@"iconserver"]];
        NSString *nsid = userDictionary[@"nsid"];

        BISUser *user = [[BISUser alloc] initWithUserName:userName urlAvatar:[BISUrlBuilder getAvatarUrl:iconfarm iconserver:iconserver nsidOrAuthor:nsid]];
        [whoLiked addObject:user];
    }
    BISPicture *updatedPicture = picture;
    updatedPicture.whoLiked = [whoLiked copy];
    updatedPicture.countLikes = [countFavorites integerValue];
    return updatedPicture;
}

+ (BISPicture *)getPictureWithUpdateComments:(BISPicture *)picture fromDict:(NSDictionary *)dictionary {
    NSArray *commentsArray = dictionary[@"comments"][@"comment"];

    NSMutableDictionary < BISUser *, NSString * > *comments = [NSMutableDictionary new];
    for (NSDictionary *commentDictionary in commentsArray) {
        NSString *userName = commentDictionary[@"authorname"];
        NSString *iconfarm = [NSString stringWithFormat:@"%@", commentDictionary[@"iconfarm"]];
        NSString *iconserver = [NSString stringWithFormat:@"%@", commentDictionary[@"iconserver"]];
        NSString *author = commentDictionary[@"author"];

        NSString *commentContent = commentDictionary[@"_content"];

        BISUser *user = [[BISUser alloc] initWithUserName:userName urlAvatar:[BISUrlBuilder getAvatarUrl:iconfarm iconserver:iconserver nsidOrAuthor:author]];
        comments[user] = commentContent;
    }
    BISPicture *updatedPicture = picture;
    updatedPicture.comments = comments;

    return updatedPicture;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"location = %@, \n owner = %@,\n countComments = %lu,\n countLikes = %lu, \n",_location,_owner.username,(unsigned long)_countComments,(unsigned long)_countLikes];
}

@end
