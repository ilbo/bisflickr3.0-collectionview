//
//  BISNavigationCollection.m
//  BISFlickr3.0
//
//  Created by iliya on 30.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISNavigationCollection.h"
#import "BISCollectionController.h"

@interface BISNavigationCollection ()
@property(nonatomic, strong) NSString *apiKey;
@end

@implementation BISNavigationCollection

- (instancetype)initWithApiKey:(NSString *)apiKey {
    self = [super init];
    if (self) {
        _apiKey = apiKey;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    BISCollectionController *collectionController = [[BISCollectionController alloc] initWithApiKey:_apiKey];
    self.viewControllers = @[collectionController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
