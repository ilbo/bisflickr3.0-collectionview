//
//  BISTypedef.h
//  BISFlickr3.0
//
//  Created by iliya on 08.07.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class BISPicture;

@interface BISTypedef : NSObject

/**
 Список типов обновленных данных в объекте BISPicture, возвращенных в блок
 
 - BISTypeUpdatedDataOwnerAndCountComments: данные о владельце, кол-во комментов
 - BISTypeUpdatedDataWhoLikesAndCountLikes: кто лайкнул, кол-во лайков
 - BISTypeUpdatedDataComments: данные о комментариях
 */
typedef NS_ENUM(NSInteger, BISTypeUpdatedData) {
    BISTypeUpdatedDataOwnerAndCountComments,
    BISTypeUpdatedDataWhoLikesAndCountLikes,
    BISTypeUpdatedDataComments
};

/**Блок для обработки данных*/
typedef void (^BISCompletionHandler)(BISPicture *, BISTypeUpdatedData, NSError *);

typedef void (^BlockCreateOwnerView) (BISPicture *, UIImage *);
typedef void (^BlockCreateCommentView) (BISPicture *, UIImage *);

@end
