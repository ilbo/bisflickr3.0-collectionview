//
//  BISUrlBuilder.h
//  BISFlickr3.0
//
//  Created by iliya on 01.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BISUrlBuilder : NSObject

- (instancetype)initWithApiKey:(NSString *)apiKey;

/**
 Формирует ссылку на фотографию
 */
+ (NSURL *)getUrlString:(NSString *)farmId serverId:(NSString *)serverId identifier:(NSString *)identifier secret:(NSString *)secret;

/**
 Формирует ссылку для загрузки аватаров
 http://farm{icon-farm}.staticflickr.com/{icon-server}/buddyicons/{nsid}.jpg
 */
+ (NSURL *)getAvatarUrl:(NSString *)iconfarm iconserver:(NSString *)iconserver nsidOrAuthor:(NSString *)nsidOrAuthor;

/**
 Формирует ссылку для запроса данных о картинке
 */
- (NSURL *)getPhotoInfoUrl:(NSString *)photoId;

/**
 Формирует ссылку для загрузки комментов к картинке
 https://api.flickr.com/services/rest/?method=flickr.photos.comments.getList&photo_id=&api_key=&format=json&nojsoncallback=1
 */
- (NSURL *)getCommentsUrl:(NSString *)photoId;

/**
 Формирует ссылку для запроса данных о лайках
 */
- (NSURL *)getPhotoFavoritesUrl:(NSString *)photoId;

/**
 Формирует ссылку из строки поиска для запроса картинки
 */
- (NSURL *)getPhotosUrl:(NSString *)searchText;

/**
 Формирует ссылку для загрузки аватара по умолчанию
 */
- (NSURL *)getByDefaultPhotoAvatarUrl;

@end
