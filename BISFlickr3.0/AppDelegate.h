//
//  AppDelegate.h
//  BISFlickr3.0
//
//  Created by iliya on 30.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property(strong, nonatomic) UIWindow *window;


@end

