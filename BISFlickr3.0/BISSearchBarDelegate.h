//
//  BISSearchBarDelegate.h
//  BISFlickr3.0
//
//  Created by iliya on 25.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BISCollectionController;

@interface BISSearchBarDelegate : NSObject <UISearchBarDelegate>

@property(nonatomic, weak) BISCollectionController *controller;
@property(nonatomic, strong) UISearchBar *searchBar;

- (instancetype)initWithController:(BISCollectionController *)controller;

/**
 Настройка и добавление поиска в контроллер
 */
- (void)addSearchBar;

@end
