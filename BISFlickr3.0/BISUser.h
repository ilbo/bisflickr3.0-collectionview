//
//  BISUser.h
//  BISFlickr3.0
//
//  Created by iliya on 25.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BISUser : NSObject <NSCopying>

/*! Имя пользователя */
@property(nonatomic, copy, readonly) NSString *username;
/*! Ссылка на аватар пользователя, содержит уникальные данные для идентификации пользователя
 http://farm{icon-farm}.staticflickr.com/{icon-server}/buddyicons/{nsid}.jpg*/
@property(nonatomic, copy, readonly) NSURL *urlAvatar;

- (instancetype)initWithUserName:(NSString *)userName urlAvatar:(NSURL *)urlAvatar;

@end
