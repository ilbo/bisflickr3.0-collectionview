//
//  BISFlickrView.h
//  BISFlickr3.0
//
//  Created by iliya on 09.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BISPicture;

@interface BISFlickrView : UIView

- (instancetype)initWithFrame:(CGRect)frame image:(UIImage *)image;

- (void)fillContentWithTitle:(NSString *)title;

- (NSString *)getTitle;

@end
