//
//  BISInfoCell.h
//  BISFlickr3.0
//
//  Created by iliya on 12.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! идентификатор ячейки */
extern NSString *const BISInfoCellIdentifier;

@interface BISInfoCell : UITableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

+ (CGFloat)heightForCell;

@end
