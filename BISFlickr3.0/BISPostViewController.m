//
//  PinchViewController.m
//  BISFlickr3.0
//
//  Created by iliya on 01.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISPostViewController.h"
#import "BISFlickrView.h"
#import "BISAvatarView.h"
#import "Masonry.h"

@interface BISPostViewController()

@property(nonatomic, weak) UIImage *image;
@property(nonatomic, weak) UIImage *avatarImage;
@property(nonatomic, weak) NSMutableDictionary *flickr;

@end

@interface BISPostViewController ()

@end

@implementation BISPostViewController

- (instancetype)initWithInfoPhoto:(NSMutableDictionary *)flickr image:(UIImage *)image avatarImage:(UIImage *)avatarImage{
    self = [super init];
    if (self){
        self.flickr = flickr;
        self.image = image;
        self.avatarImage = avatarImage;
        
    }
    return self;
}

- (void)loadView{
    BISFlickrView *flickrView = [[BISFlickrView alloc]initWithInfoPhoto:_flickr image:_image];
    self.view = flickrView;
    CGRect frame = self.navigationController.navigationBar.frame;
    BISAvatarView * avatarView = [[BISAvatarView alloc]initWithInfoPhoto:_flickr avatarImage:_avatarImage frame:frame];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-60, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.titleView = avatarView;
//    NSLog(@"---load view---\n");
    if([self isViewLoaded]){
//      NSLog(@"---isViewLoaded---\n");
    }
}

- (void)viewDidLoad{
    [super viewDidLoad];
//    NSLog(@"---viewDidLoad---\n");
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    
}

@end
