//
//  BISSharedCache.h
//  BISFlickr3.0
//
//  Created by iliya on 01.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BISSharedCache : NSObject

- (void)cacheImage:(UIImage *)image forKey:(NSURL *)key;

- (void)cacheImage:(UIImage *)image forKeyInteger:(NSInteger)key;

- (UIImage *)getCachedImageForKey:(NSURL *)key;

- (UIImage *)getCachedImageForKeyInteger:(NSInteger)key;

@end
