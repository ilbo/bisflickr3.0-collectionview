//
//  BISFlickrOperation.h
//  BISFlickr3.0
//
//  Created by iliya on 30.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class BISUrlBuilder;
@class BISPicture;

@interface BISFlickrOperation : NSOperation

- (void)finish;

- (instancetype)initWithObject:(BISPicture *)picture url:(NSURL *)url settings:(BISUrlBuilder *)settings returnObjectOrImage:(void (^)(BISPicture *, UIImage *))returnObjectOrImage queue:(NSOperationQueue *)queue;

@end
