//
//  BISPostTableViewController.m
//  BISFlickr3.0
//
//  Created by iliya on 12.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISPostTableViewController.h"
#import "BISUrlBuilder.h"
#import "BISTypedef.h"
#import "UIView+BISViewCategory.h"
#import "BISCollectionController.h"
#import "BISCollectionViewDelegate.h"
#import "BISFlickrInfoOperation.h"
#import "BISFlickrOperation.h"
#import "BISPicture.h"
#import "BISUser.h"
#import "BISSharedCache.h"
#import "BISInfoCell.h"
#import "BISFlickrView.h"
#import "BISStatisticView.h"
#import "BISCommentView.h"
#import "BISOwnerView.h"

@interface BISPostTableViewController ()

@property(nonatomic, weak) BISCollectionViewDelegate *delegate;
@property(nonatomic, assign) NSInteger indexPicture;
@property(nonatomic) CGRect frame;

@property(nonatomic, strong) NSOperationQueue *queue;
@property(nonatomic, strong) BISSharedCache *cache;
@property(nonatomic, strong) NSMutableArray<UIView *> *views;

@end

@implementation BISPostTableViewController

- (instancetype)initWithInfoDelegate:(BISCollectionViewDelegate *)delegate indexPicture:(NSInteger)indexPicture frame:(CGRect)frame {
    self = [super init];
    if (self) {
        _frame = frame;
        _delegate = delegate;
        _queue = delegate.controller.queue;
        _cache = delegate.controller.cache;
        _indexPicture = indexPicture;
        //!!!
        //[self.tableView setBackgroundColor:UIColorFromRGB];
    }
    return self;
}

- (void)dealloc {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registrationCell];
    
    self.views = [NSMutableArray new];
    
    BISPicture *picture = _delegate.controller.pictures[_indexPicture];
    UIImage *mainImage = [_cache getCachedImageForKey:picture.url];
    BISFlickrView *pictureView = [self createFlickrView:picture image:mainImage];
    _views[0] = pictureView;
    
    if(!picture.countComments){
        BISFlickrInfoOperation *operationInfoImage = [self getOperationForDownloadInformation:picture delegate:_delegate];
        [operationInfoImage setQueuePriority:NSOperationQueuePriorityVeryHigh];
        [_queue addOperation:operationInfoImage];
    }else{
        UIImage *ownerAvatar = [_cache getCachedImageForKey:picture.owner.urlAvatar];
        [self createOwnerView:picture avatar:ownerAvatar];
        
        BISStatisticView *statisticView = [self createStaticticView:picture];
        _views[1] = statisticView;
        
        for (BISUser *user in picture.comments) {
            NSString *comment = picture.comments[user];
            UIImage *avatarCommentator = [_cache getCachedImageForKey:user.urlAvatar];
            BISCommentView *commentView = [self createCommentView:user comment:comment avatar:avatarCommentator];
            [_views addObject:commentView];
        }
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _views.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            return [_views[0] heightForCell];
            
        case 1:
            return [_views[1] heightForCell];
            
        default:
            return [BISInfoCell heightForCell];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [(BISInfoCell *)cell addSubview:_views[indexPath.row]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[BISInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:BISInfoCellIdentifier];
    return cell;
}

/**
 Создает операцию для скачивания аватарок, комментариев и информации о картинке
 */
- (BISFlickrInfoOperation *)getOperationForDownloadInformation:(BISPicture *)picture delegate:(BISCollectionViewDelegate *)delegate {
    
    BISCompletionHandler completionHandler = ^(BISPicture *pictureFromBlock, BISTypeUpdatedData typeUpdatedData, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSMutableArray *mutablePictures = [delegate.controller.pictures mutableCopy];
            mutablePictures[_indexPicture] = pictureFromBlock;
            delegate.controller.pictures = [mutablePictures copy];
            
            //пришла информация о владельце и кол-ве комментов
            if(typeUpdatedData == BISTypeUpdatedDataOwnerAndCountComments){
                
                
                //качаем аватар владельца
                NSURL *url = pictureFromBlock.owner.urlAvatar;
                BISUser *owner = pictureFromBlock.owner;
                BlockCreateOwnerView blockCreateOwnerView = ^(BISPicture *pictureFromBlock, UIImage *image) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (image) {
                            if (![_cache getCachedImageForKey:url]) {
                                [_cache cacheImage:image forKey:url];
                            }
                            [self createOwnerView:delegate.controller.pictures[_indexPicture] avatar:image];
                            [self.tableView reloadData];
                        }
                    });
                };
                BISFlickrOperation *operationForDownloadOwnersAvatar = [self getOperationForDownloadAvatar:owner returnObjectOrImage:blockCreateOwnerView];
                [_queue addOperation:operationForDownloadOwnersAvatar];
            }
            //пришла информация о кол-ве лайков
            if(typeUpdatedData == BISTypeUpdatedDataWhoLikesAndCountLikes){
                
                BISStatisticView *statisticView = [self createStaticticView:pictureFromBlock];
                _views[1] = statisticView;
                [self.tableView reloadData];
            }
            
            //пришла информация о комментариях
            if(typeUpdatedData == BISTypeUpdatedDataComments){
                NSDictionary <BISUser *, NSString *> *comments = pictureFromBlock.comments;
                if (comments == 0) {
                    return;
                }
                for (BISUser *user in comments) {
                    NSString *comment = pictureFromBlock.comments[user];
                    
                    NSURL *url = user.urlAvatar;
                    BlockCreateCommentView blockCreateCommentView = ^(BISPicture *pictureFromBlock, UIImage *image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (image) {
                                if (![_cache getCachedImageForKey:url]) {
                                    [_cache cacheImage:image forKey:url];
                                }
                                BISCommentView *commentView = [self createCommentView:user comment:comment avatar:image];
                                [_views addObject:commentView];
                                [self.tableView reloadData];
                            }
                        });
                    };
                    
                    BISFlickrOperation *operationForDownloadCommentatorsAvatars = [self getOperationForDownloadAvatar:user returnObjectOrImage:blockCreateCommentView];
                    [_queue addOperation:operationForDownloadCommentatorsAvatars];
                }
            }
        });
        self.delegate = nil;
    };
    
    BISFlickrInfoOperation *operation = [[BISFlickrInfoOperation alloc] initWithObject:picture settings:delegate.controller.settings completionHandler:completionHandler];
    return operation;
}

/**
 Создает операцию для скачивания аватарок, комментирующих картинку
 */
- (BISFlickrOperation *)getOperationForDownloadAvatar:(BISUser *)user returnObjectOrImage:(void(^)(BISPicture *, UIImage *))returnObjectOrImage{
    
    NSURL *url = user.urlAvatar;
    
    BISFlickrOperation *operation = [[BISFlickrOperation alloc] initWithObject:nil url:url settings:_delegate.controller.settings returnObjectOrImage:returnObjectOrImage queue:_queue];
    return operation;
}

- (void)registrationCell {
    [self.tableView registerClass:[BISInfoCell class] forCellReuseIdentifier:BISInfoCellIdentifier];
}

- (BISFlickrView *)createFlickrView:(BISPicture *)picture image:(UIImage *)image {
    BISFlickrView *flickrView = [[BISFlickrView alloc] initWithFrame:self.frame image:image];
    [flickrView fillContentWithTitle:picture.title];
    return flickrView;
}

- (BISStatisticView *)createStaticticView:(BISPicture *)picture {
    BISStatisticView *statView = [[BISStatisticView alloc] initWithCountLikes:picture.countLikes countComments:picture.countComments];
    [statView fillContent];
    return statView;
}

- (BISCommentView *)createCommentView:(BISUser *)user comment:(NSString *)comment avatar:(UIImage *)avatar {
    NSAttributedString * commentAttrStr = [[NSAttributedString alloc] initWithData:[comment dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    BISCommentView *commentView = [[BISCommentView alloc]initWithFrame:self.view.frame];
    [commentView fillContentWithUserName:user.username comment:commentAttrStr avatar:avatar];
    return commentView;
}

- (void)createOwnerView:(BISPicture *)picture avatar:(UIImage *)avatar {
    BISOwnerView *ownerView = [[BISOwnerView alloc] initWithUserName:picture.owner.username location:picture.location avatar:avatar frame:_frame];
    [ownerView fillContent];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-60, -60) forBarMetrics:UIBarMetricsDefault];
    [self.navigationItem setTitleView:ownerView];
}

@end
