//
//  BISAvatarView.m
//  BISFlickr3.0
//
//  Created by iliya on 10.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "UIColor+BISUIColorCategory.h"
#import "BISOwnerView.h"
#import "BISPicture.h"
#import "Masonry.h"

@interface BISOwnerView ()

@property(nonatomic, strong) UIImageView *avatarViewImage;
@property(nonatomic, strong) UILabel *userNameLabel;
@property(nonatomic, strong) UILabel *locationLabel;
@property(nonatomic, strong) UIImageView *viewImageLocation;
@property(nonatomic, weak) UIImage *avatar;

@property(nonatomic, copy, readonly) NSString *userName;
@property(nonatomic, copy, readonly) NSString *location;

@end

@implementation BISOwnerView

- (instancetype)initWithUserName:(NSString *)userName location:(NSString *)location avatar:(UIImage *)avatar frame:(CGRect)frame {
    self = [super init];
    if (self) {
        self.frame = frame;
        _avatar = avatar;
        _userName = userName;
        _location = location;
        [self initElements];
    }
    return self;
}

- (void)initElements {

    int padding = 5.0;
    UIView *superview = self;

    _avatarViewImage = [[UIImageView alloc] initWithImage:_avatar];
    _avatarViewImage.contentMode = UIViewContentModeScaleAspectFit;
    _avatarViewImage.layer.cornerRadius = _avatarViewImage.frame.size.height / 2;
    _avatarViewImage.clipsToBounds = YES;
    [self addSubview:_avatarViewImage];

    _userNameLabel = [UILabel new];
    _userNameLabel.font = [UIFont systemFontOfSize:12.0 weight:UIFontWeightHeavy];
    _userNameLabel.textAlignment = NSTextAlignmentLeft;
    [_userNameLabel sizeToFit];

    _userNameLabel.textColor = [UIColor blackColor];
    [self addSubview:_userNameLabel];

    _locationLabel = [UILabel new];
    _locationLabel.font = [UIFont systemFontOfSize:12.0 weight:UIFontWeightHeavy];
    _locationLabel.textAlignment = NSTextAlignmentLeft;
    [_locationLabel sizeToFit];

    _locationLabel.textColor = [UIColor bisTextColorWithAlpha:1.0];
    [self addSubview:_locationLabel];

    _viewImageLocation = [UIImageView new];
    _viewImageLocation.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_viewImageLocation];


    [_avatarViewImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).offset(1);
        make.left.equalTo(superview.mas_left).offset(padding);
    }];


    [_userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).offset(padding);
        make.left.equalTo(superview.mas_left).with.offset(60);
        make.right.equalTo(superview.mas_right).with.offset(-padding);
    }];

    [_locationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_userNameLabel.mas_bottom).offset(padding);
        make.left.equalTo(superview.mas_left).with.offset(73);
        make.right.equalTo(superview.mas_right).with.offset(-padding);
    }];


    [_viewImageLocation mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_userNameLabel.mas_top).offset(20);
        make.left.equalTo(superview.mas_left).with.offset(-196);
        make.right.equalTo(_locationLabel.mas_right).with.offset(padding);
    }];
}

- (void)fillContent {
    _userNameLabel.text = _userName;

    UIImage *imageLocation = [UIImage imageNamed:@"icLocation"];
    _viewImageLocation.image = imageLocation;

    _locationLabel.text = _location;
    [self setBackgroundColor:[UIColor bisBackgroundColorWithAlpha:1.0]];
}

- (NSString *)getUserName {
    return _userNameLabel.text;
}

@end
