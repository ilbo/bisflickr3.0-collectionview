//
//  UIView+BISViewCategory.h
//  BISFlickr3.0
//
//  Created by iliya on 18.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BISViewCategory)

- (CGFloat)heightForCell;

@end
