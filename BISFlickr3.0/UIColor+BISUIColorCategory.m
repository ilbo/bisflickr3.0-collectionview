//
//  UIColor+BISUIColorCategory.m
//  BISFlickr3.0
//
//  Created by iliya on 24.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "UIColor+BISUIColorCategory.h"

@implementation UIColor (BISUIColorCategory)

+ (UIColor *)bisBackgroundColorWithAlpha:(float)alpha {
    return [UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:250.0/255.0 alpha:alpha];
}

+ (UIColor *)bisTextColorWithAlpha:(float)alpha {
    return [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:alpha];
}

+ (UIColor *)bisSearchColorWithAlpha:(float)alpha {
    return [UIColor colorWithRed:234.0f / 255.0f green:234.0f / 255.0f blue:234.0f / 255.0f alpha:alpha];
}

@end
