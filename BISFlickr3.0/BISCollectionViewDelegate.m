//
//  BISCollectionViewDelegate.m
//  BISFlickr3.0
//
//  Created by iliya on 25.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISCollectionViewDelegate.h"
#import "BISCollectionController.h"
#import "BISPostTableViewController.h"
#import "BISFlickrOperation.h"
#import "BISPicture.h"
#import "BISSharedCache.h"
#import "BISFlickrCell.h"

@implementation BISCollectionViewDelegate

- (instancetype)initWithController:(BISCollectionController *)controller {
    self = [super init];
    if (self) {
        _controller = controller;
    }
    return self;
}


/**
 Метод перед отрисовкой каждой ячейки
 
 *Если не работает операция для скачивания и не загружена даннаяя фотка - создаем операцию
 блок возвращает
 или прогресс, тогда присваиваем прогресс ячейке
 или фотку, тогда добавляем в кэш и присваиваем ячейке
 
 *Если не работает операция для скачивания и фотография загружена - берем из кэша
 
 @param collectionView default
 @param indexPath default
 */
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    BISFlickrCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:BISFlickrCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[BISFlickrCell alloc] initWithFrame:_controller.collectionView.frame];
    }
    BISPicture *picture = _controller.pictures[indexPath.row];

#pragma mark - Run operation for download images

    NSURL *url = picture.url;
    NSInteger index = picture.index;

    //если не создавалась операция для скачивания картинки, то создаем операцию

    if (!_controller.executingOperations[indexPath] && ![_controller.cache getCachedImageForKey:url]) {

        BISFlickrOperation *operation = [[BISFlickrOperation alloc] initWithObject:picture url:url settings:_controller.settings returnObjectOrImage:^(BISPicture *pictureFromBlock, UIImage *image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (pictureFromBlock.downloadState == BISDownloadStateDownloading) {

                    float progress = pictureFromBlock.progress;
                    NSString *size = pictureFromBlock.size;
                    _controller.pictures[index].progress = progress;
                    _controller.pictures[index].size = size;
                    [(BISFlickrCell *) cell addProgress:progress size:size];

                }
                if (pictureFromBlock.downloadState == BISDownloadStateDownloaded && image) {

                    if ([_controller.cache getCachedImageForKey:url] == nil) {
                        [_controller.cache cacheImage:image forKey:url];
                        [(BISFlickrCell *) cell addImage:image];
                    }
                    [_controller.executingOperations removeObjectForKey:indexPath];
                }
                if (pictureFromBlock.downloadState == BISDownloadStateError) {
                    //обработать ошибку
                }

            });
        }queue:_controller.queue];
        _controller.executingOperations[indexPath] = operation;
        [_controller.queue addOperation:operation];

    } else {
        UIImage *foundImageInCache = [_controller.cache getCachedImageForKey:picture.url];
        [(BISFlickrCell *) cell addImage:foundImageInCache];
    }
    return cell;
}

/**
 Обработка события при нажатии на картинку
 */
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    BISPicture *picture = _controller.pictures[indexPath.row];

    UIImage *image = [_controller.cache getCachedImageForKey:picture.url];
    if (image == nil) {
        return;
    }
    CGRect frame = _controller.navigationController.navigationBar.frame;
    BISPostTableViewController *postTVC = [[BISPostTableViewController alloc] initWithInfoDelegate:self indexPicture:indexPath.row frame:frame];
    
    [_controller.navigationController pushViewController:postTVC animated:NO];
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {

    CGFloat leftRightInset = 0.0f;
    return UIEdgeInsetsMake(0, leftRightInset, 0, leftRightInset);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _controller.pictures.count;
}

@end
