//
//  BISStaticticView.m
//  BISFlickr3.0
//
//  Created by iliya on 12.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "UIColor+BISUIColorCategory.h"
#import "BISStatisticView.h"
#import "Masonry.h"

@interface BISStatisticView ()

@property(nonatomic, assign, readonly) NSInteger countLikes;
@property(nonatomic, assign, readonly) NSInteger countComments;

@property(nonatomic, strong) UIImageView *iconFavorites;
@property(nonatomic, strong) UILabel *amountOfFavorites;
@property(nonatomic, strong) UIImageView *iconComments;
@property(nonatomic, strong) UILabel *amountOfComments;
@property(nonatomic) CGRect frame;

@end

@implementation BISStatisticView

- (instancetype)initWithCountLikes:(NSInteger)countLikes countComments:(NSInteger)countComments {
    self = [super init];
    if (self) {
        [self initElements];
        _countLikes = countLikes;
        _countComments = countComments;
    }
    return self;
}

- (void)initElements {

    int padding = 15.0;
    UIView *superview = self;

    _iconFavorites = [UIImageView new];
    [self addSubview:_iconFavorites];

    _iconComments = [UIImageView new];
    [self addSubview:_iconComments];

    _amountOfFavorites = [UILabel new];
    UIFont *font = [UIFont systemFontOfSize:15.0];
    _amountOfFavorites.font = font;
    _amountOfFavorites.textAlignment = NSTextAlignmentLeft;
    [_amountOfFavorites setNumberOfLines:0];
    [_amountOfFavorites sizeToFit];

    _amountOfFavorites.textColor = [UIColor bisTextColorWithAlpha:1.0];
    [self addSubview:_amountOfFavorites];


    _amountOfComments = [UILabel new];
    _amountOfComments.font = font;
    _amountOfComments.textAlignment = NSTextAlignmentLeft;
    [_amountOfComments setNumberOfLines:0];
    [_amountOfComments sizeToFit];

    _amountOfComments.textColor = [UIColor bisTextColorWithAlpha:1.0];
    [self addSubview:_amountOfComments];


    [_iconFavorites mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superview.mas_right).offset(padding);
    }];

    [_amountOfFavorites mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_iconFavorites.mas_right).offset(padding);
        //        make.left.equalTo(superview.mas_left).offset(padding);
        //        make.right.equalTo(superview.mas_right).offset(-padding);
    }];

    [_iconComments mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_amountOfFavorites.mas_right).offset(50.0);
    }];

    [_amountOfComments mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_iconComments.mas_right).offset(padding);
    }];
}

- (void)setAmountOfFavorites:(NSString *)countFavorites {

}

- (void)setAmountOfComments:(NSString *)countComments {

}

- (UILabel *)getLabelOfFavorites {
    return _amountOfFavorites;
}

- (UILabel *)getLabelOfComments {
    return _amountOfComments;
}

- (NSString *)getAmountOfFavorites {
    return _amountOfFavorites.text;
}

- (NSString *)getAmountOfComments {
    return _amountOfComments.text;
}

- (CGFloat)heightForCell {
    return 40;
}

- (void)fillContent {
    UIImage *btnFaves = [UIImage imageNamed:@"btnFaves"];
    _iconFavorites.image = btnFaves;

    UIImage *btnComments = [UIImage imageNamed:@"btnComments"];
    _iconComments.image = btnComments;

    NSString *likesText = [NSString stringWithFormat:@"%ld лайка", (long) _countLikes];
    _amountOfFavorites.text = likesText;

    NSString *commentsText = [NSString stringWithFormat:@"%ld комментариев", (long) _countComments];
    _amountOfComments.text = commentsText;

    [self setBackgroundColor:[UIColor bisBackgroundColorWithAlpha:1.0]];
}

@end
