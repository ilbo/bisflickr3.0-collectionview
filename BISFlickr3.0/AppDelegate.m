//
//  AppDelegate.m
//  BISFlickr3.0
//
//  Created by iliya on 30.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "AppDelegate.h"
#import "BISCollectionController.h"
#import "BISFavouritesTableViewController.h"
#import "BISNavigationCollection.h"


@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    const char *API_KEY = [@"95a9d0421dad17376fce473c8726e656" cStringUsingEncoding:NSUTF8StringEncoding];

    UITabBarController *tabBarController = [UITabBarController new];
    BISNavigationCollection *collectionController = [[BISNavigationCollection alloc] initWithApiKey:[NSString stringWithFormat:@"%s", API_KEY]];
    UIImage *feed = [UIImage imageNamed:@"icFeed"];
    collectionController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Лента" image:feed tag:0];
    collectionController.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -5.0);

    UINavigationController *favouritesController = [[UINavigationController alloc] initWithRootViewController:[BISFavouritesTableViewController new]];
    UIImage *likes = [UIImage imageNamed:@"icFavourite"];
    favouritesController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Избранное" image:likes tag:0];
    favouritesController.tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -5.0);

    tabBarController.viewControllers = @[collectionController, favouritesController];
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
}


- (void)applicationWillTerminate:(UIApplication *)application {
}


@end
