//
//  BISCollectionViewDelegate.h
//  BISFlickr3.0
//
//  Created by iliya on 25.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@class BISCollectionController;

@interface BISCollectionViewDelegate : NSObject <UICollectionViewDelegate, UICollectionViewDataSource>

@property(nonatomic, weak) BISCollectionController *controller;

- (instancetype)initWithController:(BISCollectionController *)controller;

@end
