//
//  BISFlickrInfoOperaton.m
//  BISFlickr3.0
//
//  Created by iliya on 11.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISFlickrInfoOperation.h"
#import "BISPicture.h"
#import "BISUser.h"
#import "BISUrlBuilder.h"
#import "BISSessionDataTaskService.h"

@interface BISFlickrInfoOperation ()

@property(nonatomic, strong) BISUrlBuilder *settings;
@property(nonatomic, strong) NSURLSession *session;
@property(nonatomic, strong) BISCompletionHandler completionHandler;
@property(nonatomic, strong) BISPicture *picture;

@end

@implementation BISFlickrInfoOperation

@synthesize ready = _ready;
@synthesize executing = _executing;
@synthesize finished = _finished;

#pragma mark - Init

- (instancetype)initWithObject:(BISPicture *)picture settings:(BISUrlBuilder *)settings completionHandler:(BISCompletionHandler)completionHandler {
    self = [super init];
    if (self) {
        _ready = YES;
        _settings = settings;
        _picture = picture;
        _completionHandler = completionHandler;
    }
    return self;
}

#pragma mark - Main

- (void)main {
    
    @try {
        @autoreleasepool {
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            //скачиваем инфо о картинке: location,countComments
            //и владельце: username,nsid,iconserver,iconfarm
            
            NSString *pictureId = [NSString stringWithFormat:@"%ld", (long) _picture.identifier];
            NSURL *urlPhotosGetInfo = [_settings getPhotoInfoUrl:pictureId];
            BISSessionDataTaskService *dataTaskService = [BISSessionDataTaskService new];
            
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            [dataTaskService getCommonDictionaries:urlPhotosGetInfo returnInBlock:^(NSDictionary *dictionary, NSError *error) {
                
                BISPicture *updatedPicture = [BISPicture getPictureWithUpdateInfo:_picture fromDict:dictionary];
                _picture = updatedPicture;
                
                dispatch_semaphore_signal(sema);
                _completionHandler(_picture, BISTypeUpdatedDataOwnerAndCountComments, error);
            }];
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
            
            
            //скачиваем инфо о лайках: personsWhoLikes(массив), countLikes
            
            NSURL *urlPhotosGetFavorites = [_settings getPhotoFavoritesUrl:pictureId];
            sema = dispatch_semaphore_create(0);
            [dataTaskService getCommonDictionaries:urlPhotosGetFavorites returnInBlock:^(NSDictionary *dictionary, NSError *error) {
                
                BISPicture *updatedPicture = [BISPicture getPictureWithUpdateFavorites:_picture fromDict:dictionary];
                _picture = updatedPicture;
                
                dispatch_semaphore_signal(sema);
                _completionHandler(_picture, BISTypeUpdatedDataWhoLikesAndCountLikes, error);
            }];
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
            
            
            NSString *countComments = [NSString stringWithFormat:@"%lu", (unsigned long) _picture.countComments];
            if (![countComments isEqualToString:@"0"]) {
                
                //скачиваем комменты к картинке
                
                sema = dispatch_semaphore_create(0);
                NSURL *urlCommentsGetList = [_settings getCommentsUrl:pictureId];
                [dataTaskService getCommonDictionaries:urlCommentsGetList returnInBlock:^(NSDictionary *dictionary, NSError *error) {
                    
                    BISPicture *updatedPicture = [BISPicture getPictureWithUpdateComments:_picture fromDict:dictionary];
                    _picture = updatedPicture;
                    
                    dispatch_semaphore_signal(sema);
                    _completionHandler(_picture, BISTypeUpdatedDataComments, error);
                    [self finish];
                }];
                dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
                
            } else {
                [self finish];
                //0 Comments, break infoOperation
            }
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception in Operation %@", e);
    }
}

#pragma mark - Control

- (void)finish {
    if (self.executing) {
        self.executing = NO;
        self.finished = YES;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    }
}

#pragma mark - State

- (void)setReady:(BOOL)ready {
    if (_ready != ready) {
        [self willChangeValueForKey:NSStringFromSelector(@selector(isReady))];
        _ready = ready;
        [self didChangeValueForKey:NSStringFromSelector(@selector(isReady))];
    }
}

- (BOOL)isReady {
    return _ready;
}

- (void)setExecuting:(BOOL)executing {
    
    if (_executing != executing) {
        [self willChangeValueForKey:NSStringFromSelector(@selector(isExecuting))];
        _executing = executing;
        [self didChangeValueForKey:NSStringFromSelector(@selector(isExecuting))];
    }
}

- (BOOL)isExecuting {
    return _executing;
}

- (void)setFinished:(BOOL)finished {
    if (_finished != finished) {
        [self willChangeValueForKey:NSStringFromSelector(@selector(isFinished))];
        _finished = finished;
        [self didChangeValueForKey:NSStringFromSelector(@selector(isFinished))];
    }
}

- (BOOL)isFinished {
    
    return _finished;
}

#pragma mark - Cancel

- (void)cancel {
    
    [super cancel];
    [self finish];
}

@end
