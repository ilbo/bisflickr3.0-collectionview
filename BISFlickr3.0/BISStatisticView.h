//
//  BISStaticticView.h
//  BISFlickr3.0
//
//  Created by iliya on 12.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BISStatisticView : UIView

- (instancetype)initWithCountLikes:(NSInteger)countLikes countComments:(NSInteger)countComments;

- (UILabel *)getLabelOfFavorites;

- (NSString *)getAmountOfFavorites;

- (UILabel *)getLabelOfComments;

- (NSString *)getAmountOfComments;

- (void)fillContent;

- (CGFloat)heightForCell;

@end
