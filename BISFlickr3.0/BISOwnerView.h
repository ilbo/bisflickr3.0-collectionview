//
//  BISAvatarView.h
//  BISFlickr3.0
//
//  Created by iliya on 10.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BISPicture;

@interface BISOwnerView : UIView

- (instancetype)initWithUserName:(NSString *)userName location:(NSString *)location avatar:(UIImage *)avatar frame:(CGRect)frame;

- (void)fillContent;

- (NSString *)getUserName;

@end
