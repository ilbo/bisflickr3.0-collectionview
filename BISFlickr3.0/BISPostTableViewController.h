//
//  BISPostTableViewController.h
//  BISFlickr3.0
//
//  Created by iliya on 12.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BISCollectionViewDelegate;
@class BISSharedCache;
@class BISUrlBuilder;
@class BISFlickrs;
@class BISPicture;

@interface BISPostTableViewController : UITableViewController

/*! @abstract Use initWithDelegate: to init with a delegate. */
- (instancetype)init NS_UNAVAILABLE;

//- (instancetype)initWithDelegate:(id<?>)delegate;

- (instancetype)initWithInfoDelegate:(BISCollectionViewDelegate *)delegate indexPicture:(NSInteger)indexPicture frame:(CGRect)frame;

@end
