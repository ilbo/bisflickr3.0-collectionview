//
//  BISFlickrCell.h
//  BISFlickr3.0
//
//  Created by iliya on 01.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BISPicture;

extern NSString *const BISFlickrCellIdentifier;

@interface BISFlickrCell : UICollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame;

- (UIImage *)getImage;

- (void)addImage:(UIImage *)image;

- (void)addProgress:(float)progress size:(NSString *)size;

+ (CGFloat)heightForCell;

@end
