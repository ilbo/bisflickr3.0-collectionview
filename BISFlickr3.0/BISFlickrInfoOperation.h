//
//  BISFlickrInfoOperaton.h
//  BISFlickr3.0
//
//  Created by iliya on 11.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BISTypedef.h"
#import <UIKit/UIKit.h>

@class BISUrlBuilder;
@class BISPicture;

@interface BISFlickrInfoOperation : NSOperation

- (void)finish;

- (instancetype)initWithObject:(BISPicture *)picture settings:(BISUrlBuilder *)settings completionHandler:(BISCompletionHandler)completionHandler;

@end
