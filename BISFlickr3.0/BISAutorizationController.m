//
//  BISAutorizationController.m
//  BISFlickr3.0
//
//  Created by iliya on 09.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//


#import <WebKit/WebKit.h>
#import "BISAutorizationController.h"

//static NSString *const BISAuthURL = @"https://www.flickr.com/services/oauth/authorize?oauth_token=95a9d0421dad17376fce473c8726e656";


//static NSString *const BISAuthURL = @"https://www.flickr.com/auth-72157681826748904";
static NSString *const BISAuthURL =@"https://www.flickr.com/services/oauth/authorize?oauth_token=72157681892896862-568797c5da3cf4a3&perms=delete";

@interface BISAutorizationController () <WKNavigationDelegate>
@property (nonatomic,strong) WKWebView *webView;
@end

@implementation BISAutorizationController

- (void)viewDidLoad {
    [super viewDidLoad];
    if(!self.webView)
    {
        CGRect rect =  CGRectMake(0, 80, self.view.bounds.size.width, self.view.bounds.size.height-80);
        self.webView = [[WKWebView alloc]initWithFrame: rect];
        self.webView.navigationDelegate=self;
        [self.view addSubview:self.webView];
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    }
    
    NSURLRequest *nsurlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:BISAuthURL]];
    [self.webView loadRequest:nsurlRequest];
}

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    NSLog(@"didFinishNavigation");
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if (![self.webView.URL.absoluteString isEqualToString:BISAuthURL]) {
        
        NSString *url = self.webView.URL.absoluteString;
        NSRegularExpression *tokenParseExpression = [NSRegularExpression regularExpressionWithPattern:@"access_token=(.*?)&"
                                                                                              options:NSRegularExpressionCaseInsensitive error:nil];
        
        NSArray *matches = [tokenParseExpression matchesInString:url
                                                         options:0
                                                           range:NSMakeRange(0, url.length)];
        NSTextCheckingResult *result  = matches.firstObject;
        
        NSString *accessToken = nil;
        if (result) {
            accessToken = [url substringWithRange:[result rangeAtIndex:1]];
            //[CBAuthToken saveAccessToken:accessToken];
            NSLog(@"Access token %@", accessToken);
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

@end
