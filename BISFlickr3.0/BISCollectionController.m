//
//  BISCollectionViewController.m
//  BISFlickr3.0
//
//  Created by iliya on 30.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISCollectionController.h"
#import "BISPicture.h"
#import "UIColor+BISUIColorCategory.h"
#import "BISPostTableViewController.h"
#import "BISCollectionViewDelegate.h"
#import "BISSearchBarDelegate.h"
#import "BISPostViewController.h"
#import "BISSessionDataTaskService.h"
#import "BISFlickrInfoOperation.h"
#import "BISFlickrOperation.h"
#import "BISUserDefaults.h"
#import "BISUrlBuilder.h"
#import "BISSharedCache.h"
#import "BISFlickrCell.h"
#import <WebKit/WebKit.h>
#import "Masonry.h"

@interface BISCollectionController () <UICollectionViewDelegateFlowLayout>

@property(nonatomic, strong) NSString *apiKey;

/*!сервис для загрузки общих данных*/
@property(nonatomic, strong) BISSessionDataTaskService *dataTaskService;

@property(nonatomic, strong) BISSearchBarDelegate *searchBarDelegate;
@property(nonatomic, strong) id <UICollectionViewDelegate, UICollectionViewDataSource> collectionViewDelegate;

@end

@implementation BISCollectionController

- (instancetype)initWithApiKey:(NSString *)apiKey {
    self = [super init];
    if (self) {
        _apiKey = apiKey;
        _searchBarDelegate = [[BISSearchBarDelegate alloc] initWithController:self];
        _collectionViewDelegate = [[BISCollectionViewDelegate alloc] initWithController:self];
        
        _pictures = [NSArray new];
        _dataTaskService = [BISSessionDataTaskService new];
        _settings = [[BISUrlBuilder alloc] initWithApiKey:apiKey];
        
        _queue = [NSOperationQueue new];
        _cache = [BISSharedCache new];
        _executingOperations = [NSMutableDictionary new];
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [_searchBarDelegate addSearchBar];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:[self flowLayout]];
    
    [_collectionView setDataSource:_collectionViewDelegate];
    [_collectionView setDelegate:_collectionViewDelegate];
    
    [_collectionView registerClass:[BISFlickrCell class] forCellWithReuseIdentifier:BISFlickrCellIdentifier];
    
    _collectionView.backgroundColor = [UIColor bisBackgroundColorWithAlpha:1.0];
    [self.view addSubview:_collectionView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSString *lastReauestLine = [BISUserDefaults getLastRequestLine];
    if(lastReauestLine){
        _searchBarDelegate.searchBar.text = lastReauestLine;
        [self sendCommonRequest:[self getOptimizedSearchText:lastReauestLine]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    _queue = [NSOperationQueue new];
    _cache = [BISSharedCache new];
    _pictures = [NSArray new];
}

/**
 Отправляем запрос на получение данных о картинках, создаем массив объектов
 */
- (void)sendCommonRequest:(NSString *)searchText {
    UIActivityIndicatorView *spinner = [self getSpinner];
    [spinner startAnimating];
    NSURL *url = [_settings getPhotosUrl:searchText];
    [_dataTaskService getCommonDictionaries:url returnInBlock:^(NSDictionary *dictionary, NSError *error) {
        if (!error) {
            self.pictures = [BISPicture picturesFromDictionary:dictionary];
            [_collectionView reloadData];
            [spinner stopAnimating];
        }else{
            NSLog(@"Search request receive error : %@", error.localizedDescription);
        }
    }];
}

- (NSString *)getOptimizedSearchText:(NSString *)searchText {
    NSString *optSearchText = @"";
    if (searchText) {
        NSArray *optSearchArray = [searchText componentsSeparatedByString:@" "];
        optSearchText = [optSearchArray componentsJoinedByString:@"+"];
    }
    return optSearchText;
}

- (UIActivityIndicatorView *)getSpinner {
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:spinner];
    [spinner mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
    }];
    return spinner;
}

- (UICollectionViewFlowLayout *)flowLayout {
    UICollectionViewFlowLayout *flowLayout =
    [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumLineSpacing = 1.0f;
    flowLayout.minimumInteritemSpacing = 1.0f;
    double size = self.view.bounds.size.width / 2 - 0.5f;
    flowLayout.itemSize = CGSizeMake(size, size);
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.sectionInset = UIEdgeInsetsMake(1.0f, 1.0f, 1.0f, 1.0f);
    return flowLayout;
}

@end
