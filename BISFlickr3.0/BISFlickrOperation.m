//
//  BISFlickrOperation.m
//  BISFlickr3.0
//
//  Created by iliya on 30.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISFlickrOperation.h"
#import "BISPicture.h"
#import "BISUrlBuilder.h"

@interface BISFlickrOperation () <NSURLSessionDownloadDelegate>

@property(nonatomic, strong) BISPicture *picture;
@property(nonatomic, strong) NSURL *url;
@property(nonatomic, strong) BISUrlBuilder *settings;
@property(nonatomic, strong) NSURLSession *session;
@property(nonatomic, strong) NSOperationQueue *queue;
@property(nonatomic, strong) void (^returnObjectOrImage)(BISPicture *, UIImage *);

@end

@implementation BISFlickrOperation

@synthesize ready = _ready;
@synthesize executing = _executing;
@synthesize finished = _finished;

- (instancetype)initWithObject:(BISPicture *)picture url:(NSURL *)url settings:settings returnObjectOrImage:(void (^)(BISPicture *, UIImage *))returnObjectOrImage queue:(NSOperationQueue *)queue {
    self = [super init];
    if (self) {
        _picture = picture;
        _ready = YES;
        _url = url;
        _settings = settings;
        _session = [self getSession];
        _queue = queue;
        _returnObjectOrImage = returnObjectOrImage;
    }
    return self;
}

- (NSURLSession *)getSession {
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfig.HTTPMaximumConnectionsPerHost = 40;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate:self delegateQueue:_queue];
    return session;
}

- (void)main {
    @try {
        @autoreleasepool {
            [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
            [self runTaskWithUrl:_url];
        }
    }
    @catch (NSException *e) {
        NSLog(@"Exception in Operation %@", e);
    }
}

- (void)runTaskWithUrl:(NSURL *)url {
    NSURLSessionDownloadTask *task = [_session downloadTaskWithURL:url];
    [task resume];
}


- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didFinishDownloadingToURL:(NSURL *)location {
    NSData *imageData = [NSData dataWithContentsOfURL:location];
    UIImage *image = [UIImage imageWithData:imageData];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    //если качается аватарка
    //не возвращаем процесс, для аватарок нет модели
    if(!_picture){
        _returnObjectOrImage(nil, image);
    }else{
    //если качается фотография
        _picture.downloadState = BISDownloadStateDownloaded;
        _returnObjectOrImage(_picture, image);
    }
    [self finish];
}

- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
              didCompleteWithError:(NSError *)error {
    if (error != nil) {
        if (!_picture) {
            //если ошибка скачивания аватарки
            //качаем аватар по умолчанию
            NSURL *urlPhotoAvatar = [_settings getByDefaultPhotoAvatarUrl];
            [self runTaskWithUrl:urlPhotoAvatar];
        } else {
            //если ошибка скачивания фотографии
            //возвращаем объект со статусом ошибки
            _picture.downloadState = BISDownloadStateError;
            _returnObjectOrImage(_picture, nil);
        }
        [self finish];
    }
}

- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite {
    
    float progress = (double) totalBytesWritten / (double) totalBytesExpectedToWrite;
    NSString *size = [NSByteCountFormatter stringFromByteCount:totalBytesExpectedToWrite countStyle:NSByteCountFormatterCountStyleBinary];
    //если качается аватарка
    if(!_picture){
    
    }else{
    //если качается фотография
        if (_picture.downloadState == BISDownloadStateNone) {
            _picture.downloadState = BISDownloadStateDownloading;
        }
        _picture.progress = progress;
        _picture.size = size;
        _returnObjectOrImage(_picture, nil);
    }
}

#pragma mark - Control


- (void)finish {
    if (self.executing) {
        self.executing = NO;
        self.finished = YES;
    }
}


#pragma mark - State

- (void)setReady:(BOOL)ready {
    if (_ready != ready) {
        [self willChangeValueForKey:NSStringFromSelector(@selector(isReady))];
        _ready = ready;
        [self didChangeValueForKey:NSStringFromSelector(@selector(isReady))];
    }
}

- (BOOL)isReady {
    return _ready;
}

- (void)setExecuting:(BOOL)executing {
    if (_executing != executing) {
        [self willChangeValueForKey:NSStringFromSelector(@selector(isExecuting))];
        _executing = executing;
        [self didChangeValueForKey:NSStringFromSelector(@selector(isExecuting))];
    }
}

- (BOOL)isExecuting {
    return _executing;
}

- (void)setFinished:(BOOL)finished {
    if (_finished != finished) {
        [self willChangeValueForKey:NSStringFromSelector(@selector(isFinished))];
        _finished = finished;
        [self didChangeValueForKey:NSStringFromSelector(@selector(isFinished))];
    }
}

- (BOOL)isFinished {
    return _finished;
}

#pragma mark - Cancel

- (void)cancel {
    [super cancel];
    [self finish];
}

@end
