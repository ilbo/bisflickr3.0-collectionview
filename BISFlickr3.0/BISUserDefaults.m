//
//  BISUserDefaults.m
//  BISFlickr3.0
//
//  Created by iliya on 08.07.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISUserDefaults.h"

/** Последний запрос в поисковом поле */
static NSString *const BISLastRequestLine = @"BISLastRequestLine";

@implementation BISUserDefaults

+ (nullable NSString *)getLastRequestLine {
    return [[NSUserDefaults standardUserDefaults] objectForKey:BISLastRequestLine];
}

+ (void)saveLastRequestLine:(nonnull NSString *)lastRequestLine {
    [[NSUserDefaults standardUserDefaults] setObject:lastRequestLine forKey:BISLastRequestLine];
}

@end
