//
//  BISCommentView.h
//  BISFlickr3.0
//
//  Created by iliya on 17.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BISCommentView : UIView

- (void)fillContentWithUserName:(NSString *)userName comment:(NSAttributedString *)comment avatar:(UIImage *)avatar;

- (NSString *)getUserName;

- (CGFloat)heightForCell;

@end
