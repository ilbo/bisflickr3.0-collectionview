//
//  BISCollectionViewController.h
//  BISFlickr3.0
//
//  Created by iliya on 30.05.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BISUrlBuilder;
@class BISFlickrs;
@class NSOperationQueue;
@class BISSharedCache;
@class BISPicture;

@interface BISCollectionController : UIViewController

/*! @abstract Use initWithDelegate: to init with a delegate. */
- (instancetype)init NS_UNAVAILABLE;

//- (instancetype)initWithDelegate:(id<?>)delegate;


@property(nonatomic, copy) NSArray<BISPicture *> *pictures;
@property(nonatomic, strong) NSOperationQueue *queue;
@property(nonatomic, strong) BISSharedCache *cache;
/*!Словарь с операциями с ключом indexPath для скачивания картинок*/
@property(nonatomic, strong) NSMutableDictionary *executingOperations;
/**Заменяет пробелы на плюсы*/
- (NSString *)getOptimizedSearchText:(NSString *)searchText;

- (instancetype)initWithApiKey:(NSString *)apiKey;

/*!хранит ключ и генерирует запросы*/
@property(nonatomic, strong) BISUrlBuilder *settings;
@property(nonatomic, strong) UICollectionView *collectionView;

- (void)sendCommonRequest:(NSString *)searchText;

@end
