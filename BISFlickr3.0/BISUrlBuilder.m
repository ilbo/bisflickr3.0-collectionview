//
//  BISUrlBuilder.m
//  BISFlickr3.0
//
//  Created by iliya on 01.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISUrlBuilder.h"

@interface BISUrlBuilder ()
@property(nonatomic, strong) NSString *apiKey;
@end

@implementation BISUrlBuilder

- (instancetype)initWithApiKey:(NSString *)apiKey {
    self = [super init];
    if (self) {
        _apiKey = apiKey;
    }
    return self;
}

+ (NSURL *)getUrlString:(NSString *)farmId serverId:(NSString *)serverId identifier:(NSString *)identifier secret:(NSString *)secret {
    NSMutableString *url = [NSMutableString new];
    [url appendString:@"https://farm"];
    [url appendString:farmId];
    [url appendString:@".staticflickr.com/"];
    [url appendString:serverId];
    [url appendString:@"/"];
    [url appendString:identifier];
    [url appendString:@"_"];
    [url appendString:secret];
    [url appendString:@".jpg"];
    return [NSURL URLWithString:url];
}

+ (NSURL *)getAvatarUrl:(NSString *)iconfarm iconserver:(NSString *)iconserver nsidOrAuthor:(NSString *)nsidOrAuthor {

    NSMutableString *url = [NSMutableString new];
    [url appendString:@"http://farm"];
    [url appendString:[NSString stringWithFormat:@"%@", iconfarm]];
    [url appendString:@".staticflickr.com/"];
    [url appendString:[NSString stringWithFormat:@"%@", iconserver]];
    [url appendString:@"/buddyicons/"];
    [url appendString:nsidOrAuthor];
    [url appendString:@".jpg"];
    return [NSURL URLWithString:url];
}

- (NSURL *)getPhotosUrl:(NSString *)searchText {

    NSMutableString *url = [NSMutableString new];
    [url appendString:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&text="];
    [url appendString:searchText];
    [url appendString:[NSString stringWithFormat:@"&api_key=%@&format=json&nojsoncallback=1", _apiKey]];
    return [NSURL URLWithString:url];
}

- (NSURL *)getPhotoInfoUrl:(NSString *)photoId {

    NSMutableString *url = [NSMutableString new];
    [url appendString:@"https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&photo_id="];
    [url appendString:photoId];
    [url appendString:[NSString stringWithFormat:@"&api_key=%@&format=json&nojsoncallback=1", _apiKey]];
    return [NSURL URLWithString:url];
}

- (NSURL *)getPhotoFavoritesUrl:(NSString *)photoId {

    NSMutableString *url = [NSMutableString new];
    [url appendString:@"https://api.flickr.com/services/rest/?method=flickr.photos.getFavorites&photo_id="];
    [url appendString:photoId];
    [url appendString:[NSString stringWithFormat:@"&api_key=%@&format=json&nojsoncallback=1", _apiKey]];
    return [NSURL URLWithString:url];
}

- (NSURL *)getByDefaultPhotoAvatarUrl {
    return [NSURL URLWithString:@"https://www.flickr.com/images/buddyicon.gif"];
}

- (NSURL *)getCommentsUrl:(NSString *)photoId {

    NSMutableString *url = [NSMutableString new];
    [url appendString:@"https://api.flickr.com/services/rest/?method=flickr.photos.comments.getList&photo_id="];
    [url appendString:photoId];
    [url appendString:[NSString stringWithFormat:@"&api_key=%@&format=json&nojsoncallback=1", _apiKey]];
    return [NSURL URLWithString:url];
}


@end
