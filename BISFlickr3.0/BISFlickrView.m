//
//  BISFlickrView.m
//  BISFlickr3.0
//
//  Created by iliya on 09.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "UIColor+BISUIColorCategory.h"
#import "BISPicture.h"
#import "BISFlickrView.h"
#import "Masonry.h"

@interface BISFlickrView ()

@property(nonatomic, strong) UIImageView *viewImage;
@property(nonatomic, strong) UILabel *titleLabel;
@property(nonatomic, weak) UIImage *image;

@end

@implementation BISFlickrView

- (instancetype)initWithFrame:(CGRect)frame image:(UIImage *)image{
    CGFloat width = CGRectGetWidth(frame);
    CGFloat height = image.size.height;
    self = [super initWithFrame:CGRectMake(0, 0, width, height)];
    if (self) {
        _image = image;
        [self initElements];
    }
    return self;
}

- (void)initElements {

    int padding = 1.0;
    UIView *superview = self;

    _viewImage = [[UIImageView alloc]initWithImage:_image];
    [self addSubview:_viewImage];

    _titleLabel = [UILabel new];
    UIFont *font = [UIFont systemFontOfSize:15.0];
    _titleLabel.font = font;
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    _titleLabel.numberOfLines = 0;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;

    _titleLabel.textColor = [UIColor bisTextColorWithAlpha:1.0];
    [self addSubview:_titleLabel];


    [_viewImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).offset(20.0);
        make.left.equalTo(superview.mas_left).offset(padding);
        make.right.equalTo(superview.mas_right).offset(-padding);
    }];

    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_viewImage.mas_bottom).offset(20.0);
        make.left.equalTo(superview.mas_left).with.offset(15.0);
        make.right.equalTo(superview.mas_right).offset(-15.0);
    }];
}

- (NSString *)getTitle {
    return _titleLabel.text;
}

- (CGFloat)heightForCell {
    return _image.size.height + 90;
}

- (void)fillContentWithTitle:(NSString *)title{
    _titleLabel.text = title;

    [self setBackgroundColor:[UIColor bisBackgroundColorWithAlpha:1.0]];
}

@end
