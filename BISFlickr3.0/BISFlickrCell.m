//
//  BISFlickrCell.m
//  BISFlickr3.0
//
//  Created by iliya on 01.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISFlickrCell.h"
#import "BISPicture.h"
#import "Masonry.h"

NSString *const BISFlickrCellIdentifier = @"BISFlickrCellIdentifier";

@interface BISFlickrCell ()

@property(nonatomic, strong) UIImageView *viewImage;
@property(nonatomic, strong) UILabel *progressLabel;
@property(nonatomic, strong) UIProgressView *progressView;

@end

@implementation BISFlickrCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initElements];
    }
    return self;
}

- (void)initElements {
    _viewImage = [UIImageView new];
    [self addSubview:_viewImage];
    [_viewImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
        CGFloat width = self.bounds.size.width;
        CGFloat height = self.bounds.size.height;
        make.size.mas_equalTo(CGSizeMake(width, height));
    }];

    _progressView = [UIProgressView new];
    [self addSubview:_progressView];
    [_progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
        CGFloat width = self.bounds.size.width;
        CGFloat height = self.bounds.size.height;
        make.size.mas_equalTo(CGSizeMake(width, height));
    }];

    _progressLabel = [UILabel new];
    [self addSubview:_progressLabel];
    [_progressLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_progressView);
        make.centerY.equalTo(_progressView).with.offset(-40);
    }];
}


- (UIImage *)getImage {
    return _viewImage.image;
}

- (void)addImage:(UIImage *)image {
    self.viewImage.hidden = NO;
    self.progressLabel.hidden = YES;
    self.progressView.hidden = YES;
    self.viewImage.image = image;
}

- (void)addProgress:(float)progress size:(NSString *)size {
    self.viewImage.hidden = YES;
    self.progressLabel.hidden = NO;
    self.progressView.hidden = NO;
    self.progressView.progress = progress;
    self.progressLabel.text = [NSString stringWithFormat:@"%.1f%% of %@", progress * 100, size];
}

+ (CGFloat)heightForCell {
    return 220;
}

@end
