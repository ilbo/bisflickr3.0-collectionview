//
//  BISUser.m
//  BISFlickr3.0
//
//  Created by iliya on 25.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISUser.h"

@implementation BISUser

- (instancetype)initWithUserName:(NSString *)userName urlAvatar:(NSURL *)urlAvatar {
    self = [super init];
    if (self) {
        _username = userName;
        _urlAvatar = urlAvatar;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    BISUser *user = [[[self class] allocWithZone:zone] initWithUserName:_username urlAvatar:_urlAvatar];
    return user;
}

@end
