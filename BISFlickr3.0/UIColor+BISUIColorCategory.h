//
//  UIColor+BISUIColorCategory.h
//  BISFlickr3.0
//
//  Created by iliya on 24.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (BISUIColorCategory)

+ (UIColor *)bisBackgroundColorWithAlpha:(float)alpha;
+ (UIColor *)bisTextColorWithAlpha:(float)alpha;
+ (UIColor *)bisSearchColorWithAlpha:(float)alpha;

@end
