//
//  UIView+BISViewCategory.m
//  BISFlickr3.0
//
//  Created by iliya on 18.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "UIView+BISViewCategory.h"

@implementation UIView (BISViewCategory)

- (CGFloat)heightForCell {
    return 40;
}

@end
