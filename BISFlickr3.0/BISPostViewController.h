//
//  PinchViewController.h
//  BISFlickr3.0
//
//  Created by iliya on 01.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BISPostViewController : UIViewController

- (instancetype)initWithInfoPhoto:(NSMutableDictionary *)flickr image:(UIImage *)image avatarImage:(UIImage *)avatarImage;

@end
