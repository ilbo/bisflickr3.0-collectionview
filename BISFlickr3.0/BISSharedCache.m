//
//  BISSharedCache.m
//  BISFlickr3.0
//
//  Created by iliya on 01.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISSharedCache.h"

static BISSharedCache *sharedInstance;

@interface BISSharedCache ()
@property(nonatomic, strong) NSCache *imageCache;
@end

@implementation BISSharedCache

- (instancetype)init {
    self = [super init];
    if (self) {
        self.imageCache = [[NSCache alloc] init];
    }
    return self;
}

- (void)cacheImage:(UIImage *)image forKey:(NSURL *)key {
    [self.imageCache setObject:image forKey:[key absoluteString]];
}

- (void)cacheImage:(UIImage *)image forKeyInteger:(NSInteger)key {
    [self.imageCache setObject:image forKey:[NSString stringWithFormat:@"%ld", (long) key]];
}

- (UIImage *)getCachedImageForKey:(NSURL *)key {
    return [self.imageCache objectForKey:[key absoluteString]];
}

- (UIImage *)getCachedImageForKeyInteger:(NSInteger)key {
    return [self.imageCache objectForKey:[NSString stringWithFormat:@"%ld", (long) key]];
}

@end
