//
//  BISSessionDataTaskService.h
//  BISFlickr3.0
//
//  Created by iliya on 01.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BISSessionDataTaskService : NSObject

/**
 Запускает таск и запускает обработку общих данных
 
 @param url обработанный url для запроса
 @param returnDict блок, передающий данные в table view
 */
- (void)getCommonDictionaries:(NSURL *)url returnInBlock:(void (^)(NSDictionary *dictionary, NSError *))returnDict;

@end
