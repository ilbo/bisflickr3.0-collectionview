//
//  BISCommentView.m
//  BISFlickr3.0
//
//  Created by iliya on 17.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "UIColor+BISUIColorCategory.h"
#import "BISCommentView.h"
#import "Masonry.h"

@interface BISCommentView ()

@property(nonatomic, strong) UIImageView *avatarViewImage;
@property(nonatomic, strong) UILabel *userNameLabel;
@property(nonatomic, strong) UILabel *commentLabel;

@end

@implementation BISCommentView

- (instancetype)initWithFrame:(CGRect)frame{
    CGFloat width = CGRectGetWidth(frame);
    CGFloat height = [self heightForCell];
    self = [super initWithFrame:CGRectMake(0, 0, width, height)];
    if (self) {
        [self initElements];
    }
    return self;
}

- (void)initElements {

    int padding = 15.0;
    UIView *superview = self;

    _avatarViewImage = [UIImageView new];
    _avatarViewImage.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:_avatarViewImage];

    _userNameLabel = [UILabel new];
    _userNameLabel.font = [UIFont systemFontOfSize:12.0 weight:UIFontWeightHeavy];
    _userNameLabel.textAlignment = NSTextAlignmentLeft;
    [_userNameLabel sizeToFit];

    _userNameLabel.textColor = [UIColor blackColor];
    [self addSubview:_userNameLabel];

    _commentLabel = [UILabel new];
    _commentLabel.font = [UIFont systemFontOfSize:12.0 weight:UIFontWeightHeavy];
    _commentLabel.textAlignment = NSTextAlignmentLeft;
    _commentLabel.numberOfLines = 0;
    _commentLabel.lineBreakMode = NSLineBreakByWordWrapping;

    _commentLabel.textColor = [UIColor bisTextColorWithAlpha:1.0];
    [self addSubview:_commentLabel];

    [_avatarViewImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).offset(10);
        make.left.equalTo(superview.mas_left).offset(padding);
    }];

    [_userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(superview.mas_top).offset(padding);
        make.left.equalTo(_avatarViewImage.mas_centerX).with.offset(30);
    }];

    [_commentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_userNameLabel.mas_bottom).offset(5);
        make.left.equalTo(_avatarViewImage.mas_centerX).with.offset(30);
        make.right.equalTo(superview.mas_right).with.offset(-padding);
    }];
}

- (void)fillContentWithUserName:(NSString *)userName comment:(NSAttributedString *)comment avatar:(UIImage *)avatar {
    _avatarViewImage.image = avatar;
    _userNameLabel.text = userName;
    _commentLabel.attributedText = comment;
    [self setBackgroundColor:[UIColor bisBackgroundColorWithAlpha:1.0]];
}

- (void)layoutSubviews {
    _avatarViewImage.layer.cornerRadius = _avatarViewImage.frame.size.height / 2;
    _avatarViewImage.clipsToBounds = YES;
}

- (CGFloat)heightForCell {
    return 80;
}

- (NSString *)getUserName {
    return _userNameLabel.text;
}

- (NSString *)getComment {
    return _commentLabel.text;
}

@end
