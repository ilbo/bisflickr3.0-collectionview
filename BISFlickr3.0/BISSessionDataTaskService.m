//
//  BISSessionDataTaskService.m
//  BISFlickr3.0
//
//  Created by iliya on 01.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISSessionDataTaskService.h"
#import <WebKit/WebKit.h>

@interface BISSessionDataTaskService ()
@property(nonatomic, strong) void (^returnDict)(NSDictionary *dictionary, NSError *);
@end

@implementation BISSessionDataTaskService

- (void)getCommonDictionaries:(NSURL *)url returnInBlock:(void (^)(NSDictionary *dictionary, NSError *))returnDict {
    self.returnDict = returnDict;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
    NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:^(NSData *_Nullable data, NSURLResponse *_Nullable response, NSError *_Nullable error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (error) {
            NSLog(@"Error with url: %@ \n %@", url, error.localizedDescription);
        } else {
            NSHTTPURLResponse *resp = (NSHTTPURLResponse *) response;
            if (resp.statusCode == 200) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self processingData:data compliteBlock:_returnDict];
                });
            }
        }
    }];
    [task resume];
}

/**
 Обработка общих данных
 
 @param data ...
 @param returnDict блок переносит словари с общими данными в table view
 */
- (void)processingData:(NSData *)data compliteBlock:(void (^)(NSDictionary *urlDictionaries, NSError *))returnDict {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    NSError *error = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    NSString *errorString = [dictionary valueForKey:@"error"];
    if (errorString) {
        NSLog(@"ProcessingData Error!: %@", errorString);
    }
    if (!error) {
        _returnDict(dictionary, error);
    }
}


@end
