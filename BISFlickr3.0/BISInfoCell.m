//
//  BISInfoCell.m
//  BISFlickr3.0
//
//  Created by iliya on 12.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import "BISInfoCell.h"
#import "Masonry.h"

NSString *const BISInfoCellIdentifier = @"BISInfoCellIdentifier";

@implementation BISInfoCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    return self;
}

+ (CGFloat)heightForCell {
    return 60;
}

@end
