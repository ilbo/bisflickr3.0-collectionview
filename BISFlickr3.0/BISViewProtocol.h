//
//  BISViewProtocol.h
//  BISFlickr3.0
//
//  Created by iliya on 18.06.17.
//  Copyright © 2017 iliya. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol BISViewProtocol
@required
- (CGFloat)heightForCell;
@end
